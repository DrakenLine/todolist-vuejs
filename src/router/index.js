import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/HomeView.vue'),
  },
  {
    path: '/todo',
    name: 'ToDoList',
    component: () => import('../views/TodoListView.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
